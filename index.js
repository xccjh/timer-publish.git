function getParametersSchedule(params) {
    console.log(`注意：请先尝试直接构建看看能够构建成功以保万无一失,此外,请确保计划任务期间电脑不会自动关机。`);
    const arr = process.argv.splice(2);
    try {
        let date
        if (params) {
            date = new Date(params);
        } else {
            date = new Date(arr[0] + ' ' + arr[1]);
        }
        const finishStmp = date.getTime();
        const timer = setInterval(() => {
            const currentStmp = Date.now();
            if (finishStmp > currentStmp) {
                let sec = (finishStmp - currentStmp) / 1000;
                if (sec > 60) {
                    let min = sec / 60;
                    sec = sec % 60;
                    if (min > 60) {
                        let hour = min / 60;
                        min = min % 60;
                        if (hour > 24) {
                            const day = hour / 24;
                            hour = hour % 24;
                            console.log(`还剩${parseInt(day) ? parseInt(day) + '天' : ''}${parseInt(hour) ? parseInt(hour) + '小时' : ''}${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                        } else {
                            console.log(`还剩${parseInt(hour) ? parseInt(hour) + '小时' : ''}${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                        }
                    } else {
                        console.log(`还剩${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                    }
                } else {
                    console.log(`还剩${parseInt(sec) + '秒'}开始计划任务`);
                }
            } else {
                clearInterval(timer);
            }
        }, 1000)
    } catch (e) {
        throw new Error("请指定正确的时间，示例格式：2020-10-20 10:20:40");
    }
}

function durationPointPublish(params) {
    // const schedule = require('node-schedule');
    console.log(`注意：请先尝试直接构建看看能够构建成功以保万无一失,此外,请确保计划任务期间电脑不会自动关机。`);
    try {
        let durationDay
        let durationHour
        let durationMin
        let durationSec
        if (params) {
            durationDay = params.Day;
            durationHour = params.Hour;
            durationMin = params.Min;
            durationSec = params.Sec;
        } else {
            const arr = process.argv.splice(2);
            durationDay = arr[0].split("=")[1] || 0;
            durationHour = arr[1].split("=")[1] || 0;
            durationMin = arr[2].split("=")[1] || 0;
            durationSec = arr[3].split("=")[1] || 0;
        }

        const finishStmp = Date.now()
            + durationDay * 24 * 60 * 60 * 1000
            + durationHour * 60 * 60 * 1000 + durationMin * 60 * 1000 + durationSec * 1000;
        const finish = new Date(finishStmp)

        function logTimer() {
            const currentStmp = Date.now();
            if (finishStmp > currentStmp) {
                let sec = parseInt((finishStmp - currentStmp) / 1000);
                if (sec > 60) {
                    let min = sec / 60;
                    sec = sec % 60;
                    if (min > 60) {
                        let hour = min / 60;
                        min = min % 60;
                        if (hour > 24) {
                            const day = hour / 24;
                            hour = hour % 24;
                            console.log(`还剩${parseInt(day) ? parseInt(day) + '天' : ''}${parseInt(hour) ? parseInt(hour) + '小时' : ''}${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                        } else {
                            console.log(`还剩${parseInt(hour) ? parseInt(hour) + '小时' : ''}${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                        }
                    } else {
                        console.log(`还剩${parseInt(min) ? parseInt(min) + '分钟' : ''}${parseInt(sec) ? parseInt(sec) + '秒' : ''}开始计划任务`);
                    }
                } else {
                    console.log(`还剩${parseInt(sec) + '秒'}开始计划任务`);
                }
            } else {
                clearInterval(timer);
            }
        }

        logTimer();
        const timer = setInterval(() => {
            logTimer();
        }, 1000)
    } catch (e) {
        throw new Error("请指定正确的时长，示例格式：node durationPointPublish.js Day=1 Hour=1 Min=59 Sec=59 && npm run oss:test");
    }
}

module.exports = {
    processArgvFixRun: getParametersSchedule,
    scriptArgvFixRun: (date) => getParametersSchedule(date),
    processArgvDurRun: durationPointPublish,
    scriptArgvDurRun: (date) => durationPointPublish(date)
}
