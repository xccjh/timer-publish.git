## 实现定点定时发布/构建功能的node包

1. processArgvFixRun // 定点发布，package.json传参 
2. scriptArgvFixRun // 定点发布，直接传参 
3. processArgvDurRun // 定点发布，package.json传参
4. scriptArgvDurRun // 定时发布，直接传参

### 实现效果

![实现效果](./achieveEffect.gif)

### processArgvFixRun

1. 新建fixedPointPublish.js
```js
const {processArgvFixRun} = require("@xccjh/timer-publish");
processArgvFixRun(); // 定点操作
```

2. package.json中添加
```sh
"scripts": {
    "build": "ng build --prod --aot --configuration=development",
    "fixedPointPublish": "node fixedPointPublish.js 2021-03-24 01:00:00"  👈 这个时刻执行
}
```

运行`npm run fixedPointPublish && npm run build`,构建将在2021-03-24 01:00:00时刻执行

### scriptArgvFixRun

1. 新建fixedPointPublish.js
```js
const {scriptArgvFixRun} = require("@xccjh/timer-publish");
scriptArgvFixRun('2021-03-24 01:00:00'); // 👈 这个时刻执行
```

2. package.json中添加
```sh
"scripts": {
    "build": "ng build --prod --aot --configuration=development",
    "fixedPointPublish": "node fixedPointPublish.js"
}
```
运行`npm run fixedPointPublish && npm run build`,构建将在2021-03-24 01:00:00时刻执行

### processArgvDurRun

1. 新建durationPointPublish.js
```js
const {processArgvDurRun} = require("@xccjh/timer-publish");
processArgvDurRun(); // 定时操作
```

2. package.json中添加
```sh
"scripts": {
    "build": "ng build --prod --aot --configuration=development",
    "durationPointPublish": "node durationPointPublish.js  Day=1 Hour=1 Min=59 Sec=59" 👈 1天1小时1分钟59秒后执行
}
```
运行`npm run durationPointPublish && npm run build`,构建将在1天1小时1分钟59秒后执行

### scriptArgvDurRun

1. 新建durationPointPublish.js
```js
const {scriptArgvDurRun} = require("@xccjh/timer-publish");
scriptArgvDurRun({
    Day:1,
    Hour:1,
    Min:59,
    Sec:59
}); // 👈 1天1小时1分钟59秒后执行
```

2. package.json中添加
```sh
"scripts": {
    "build": "ng build --prod --aot --configuration=development",
    "durationPointPublish": "node durationPointPublish.js"
}
```
运行`npm run durationPointPublish && npm run build`,构建将在1天1小时1分钟59秒后执行


